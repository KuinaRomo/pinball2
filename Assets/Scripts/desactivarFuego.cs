﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desactivarFuego : MonoBehaviour {

    [SerializeField] GameObject fire;
    [SerializeField] GameObject muelle;
    private bool enabledFire;
    private bool resetContador;
    private float contador;

	// Use this for initialization
	void Start () {
        enabledFire = false;
        resetContador = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (enabledFire) {
            if(contador != 0 && !resetContador) {
                contador = 0;
                resetContador = true;
            }
            contador += Time.deltaTime;
        }
        if(contador > 1) {
            resetContador = false;
            fire.SetActive(false);
            enabledFire = false;
            contador = 0;
        }
    }

    private void OnTriggerExit(Collider other){
        if(other.tag == "bola") {
            this.gameObject.GetComponent<Collider>().isTrigger = false;
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            enabledFire = true;
            muelle.GetComponent<muelle>().outBall = true;
        }
    }
}
