﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recuperarY : MonoBehaviour {

    private float positionY;
    GameObject bola;
    private bool bajarBola;
    private bool loadBall;
    private float contador;

	// Use this for initialization
	void Start () {
        //positionY = bola.transform.position.y;
        positionY = 29.3f;
        bajarBola = false;
        loadBall = false;
	}

    private void Update() {
        if(!loadBall && GameObject.FindGameObjectWithTag("bola") != null) {
            bola = GameObject.FindGameObjectWithTag("bola");
            loadBall = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (bajarBola) {
            //Debug.Log("Entro");
            if(contador > 0.06f) {
                if(bola.transform.position.y - 0.2f > positionY) {
                    bola.transform.position = new Vector3(bola.transform.position.x, bola.transform.position.y - 0.2f, bola.transform.position.z);
                }
                else {
                    bola.transform.position = new Vector3(bola.transform.position.x, positionY, bola.transform.position.z);
                    bajarBola = false;
                }
                contador = 0;
            }
            else {
                contador += Time.deltaTime;
            }
        }
	}

    private void OnTriggerEnter(Collider collision) {
        if(collision.gameObject.tag == "bola") {
            collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
            bajarBola = true;
        }
    }
}
