﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class condensadorLights : MonoBehaviour {

    [SerializeField] public Material[] lightMaterials;
    [SerializeField] GameObject parent;
    [SerializeField] GameObject GameManager;
    int contador;
    int totalChilds;
    bool allLightsOn;
    float contadorEffect;
    int iluminatedLight;
    bool resetLights;
    float contadorTime;
    private bool reseteado;

    // Use this for initialization
    void Start() {
        contador = 0;
        for(int i = 0; i < lightMaterials.Length; i++) {
            lightMaterials[i].DisableKeyword("_EMISSION");
        }
        allLightsOn = false;
        contadorEffect = 0;
        iluminatedLight = 0;
        resetLights = false;
        reseteado = false;
    }

    private void Update() {
        if(GameManager.GetComponent<GameManager>().getVidas() <= 0 && !reseteado) {
            for (int i = 0; i < lightMaterials.Length; i++) {
                lightMaterials[i].DisableKeyword("_EMISSION");
            }
            GameManager.GetComponent<GameManager>().resetLights();
            reseteado = true;
        }
        else if(GameManager.GetComponent<GameManager>().getVidas() > 0 && reseteado) {
            reseteado = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (allLightsOn) {
            contadorTime += Time.deltaTime;
            if (!resetLights) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        parent.transform.GetChild(i).GetComponent<condensadorLights>().lightMaterials[j].DisableKeyword("_EMISSION");
                    }
                }
                resetLights = true;
            }
            
            if (contadorEffect > 0.2f) {
                for(int i = 0; i < 3; i++) {
                    parent.transform.GetChild(i).GetComponent<condensadorLights>().lightMaterials[iluminatedLight].EnableKeyword("_EMISSION");
                    if(iluminatedLight != 0) {
                        parent.transform.GetChild(i).GetComponent<condensadorLights>().lightMaterials[iluminatedLight - 1].DisableKeyword("_EMISSION");
                    }
                    else {
                        parent.transform.GetChild(i).GetComponent<condensadorLights>().lightMaterials[2].DisableKeyword("_EMISSION");
                    }
                }
                contadorEffect = 0;
                if(iluminatedLight < 2) {
                    iluminatedLight++;
                }
                else {
                    iluminatedLight = 0;
                }
            }
            else {
                contadorEffect += Time.deltaTime;
            }
        }

        if(contadorTime > 2) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    parent.transform.GetChild(i).GetComponent<condensadorLights>().lightMaterials[j].DisableKeyword("_EMISSION");
                }
                parent.transform.GetChild(i).GetComponent<condensadorLights>().resetLightsOn();
            }
            GameManager.GetComponent<GameManager>().resetLights();
        }
	}

    public void resetLightsOn() {
        allLightsOn = false;
        contadorEffect = 0;
        iluminatedLight = 0;
        contador = 0;
        contadorTime = 0;
    }

    void encenderLuz() {
        lightMaterials[contador].EnableKeyword("_EMISSION");
        contador++;
        allLightsOn = GameManager.GetComponent<GameManager>().allOn();
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "bola") {
            GameManager.GetComponent<GameManager>().setScores(100);
            if (contador < lightMaterials.Length) {
                encenderLuz();
            }
        }
    }
}
