﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startGame : MonoBehaviour {

    [SerializeField] Material[] lightsOn;
    [SerializeField] GameObject GameManager;
    GameObject bola;
    bool bolaInstanciada;

	// Use this for initialization
	void Start () {
        bolaInstanciada = false;
        resetLights();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown() {
        for(int i = 0; i < lightsOn.Length; i++) {
            lightsOn[i].EnableKeyword("_EMISSION");
        }
        GameManager.GetComponent<GameManager>().setVidas(3);
        if(!bolaInstanciada) {
            bola = Instantiate(Resources.Load("prefabs/bola")) as GameObject;
            bolaInstanciada = true;
        }
        else {
            bola.SetActive(true);
        }
    }

    public void resetLights() {
        for (int i = 0; i < lightsOn.Length; i++) {
            lightsOn[i].DisableKeyword("_EMISSION");
        }
    }
}
