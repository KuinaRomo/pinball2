﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool[] targets;
    [SerializeField] string[] objectsShow;
    [SerializeField] GameObject pantalla;
    private GameObject exposed;
    public bool[] lights;
    private int scores;
    private int actualObject;
    private bool allDown;
    private bool allLightOn;
    public bool resetFirst;
    private bool inGame;
    private bool exposeFigure;
    private int highscore;
    private int vidas;
    private float timeExposed;
    private bool highscoreSuperado;
    private float vibrateTime;
    private float invisibleTime;
    private float visibleTime;
    private bool oneTimeOnly;
    private int multiplicador;
    private float timeMultiplicador;

    // Use this for initialization
    void Start() {
        targets = new bool[3];
        lights = new bool[9];
        resetFirst = false;
        resetLights();
        resetTargets();
        //resetFirst = true;
        scores = 0;
        allDown = false;
        exposeFigure = false;
        actualObject = 0;
        highscore = 100;
        scores = 0;
        vidas = 0;
        timeExposed = 0;
        highscoreSuperado = false;
        vibrateTime = 0;
        invisibleTime = 0;
        visibleTime = 0;
        multiplicador = 1;
        oneTimeOnly = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(multiplicador > 1) {
            timeMultiplicador = Time.deltaTime;
        }

        if (!highscoreSuperado) {
            GetComponent<TextMesh>().text = "Highscore: " + '\n' + highscore + '\n' + '\n' + "Scores: " + '\n' + scores + '\n' + "Life: " + vidas;
            if(scores > highscore && !oneTimeOnly) {
                highscoreSuperado = true;
            }
        }

        else if(vibrateTime < 4f && !oneTimeOnly) {
            vibrateTime += Time.deltaTime;
            if(invisibleTime > 0.3f) {
                GetComponent<TextMesh>().text = "Highscore: " + '\n' + highscore + '\n' + '\n' + '\n' + '\n' + "Life: " + vidas;
                visibleTime += Time.deltaTime;
                if(visibleTime > 0.3f) {
                    invisibleTime = 0;
                    visibleTime = 0;
                }
            }
            else {
                invisibleTime += Time.deltaTime;
                GetComponent<TextMesh>().text = "Highscore: " + '\n' + highscore + '\n' + '\n' + "Scores: " + '\n' + scores  + '\n' + "Life: " + vidas;
            }
            
        }
        else {
            highscoreSuperado = false;
            vibrateTime = 0;
            oneTimeOnly = true;
        }
       
        if(vidas <= 0) {
            if(scores > highscore) {
                highscore = scores;
            }
            scores = 0;
        }
	}

    private void FixedUpdate() {
        if (exposeFigure) {
            timeExposed += Time.deltaTime;
        }
        if(timeExposed > 6f) {
            Destroy(exposed.gameObject);
            exposeFigure = false;
            timeExposed = 0;
        }
    }

    //funciones targets
    public void resetTargets() {
        for(int i = 0; i < targets.Length; i++) {
            targets[i] = false;
        }
        if (resetFirst) {
            exposed = Instantiate(Resources.Load(objectsShow[actualObject])) as GameObject;
            if(objectsShow.Length - 1 != actualObject) {
                actualObject++;
            }
            else {
                actualObject = 0;
            }
            exposeFigure = true;
        }
        else {
            resetFirst = true;
        }
    }

    public bool putTrue() {
        allDown = true;
        for(int i = 0; i < targets.Length; i++) {
            if(!targets[i] && allDown) {
                targets[i] = true;
                if (i + 1 < targets.Length) {
                    allDown = false;
                }
                break;
            }
        }
        if(allDown == true) {
            multiplicador = 3;
        }
        return allDown;
    }


    //funciones luces condensador
    public void resetLights() {
        for(int i = 0; i < lights.Length; i++) {
            lights[i] = false;
        }
        //lights[lights.Length - 1] = false;
    }

    public bool allOn() {
        allLightOn = true;
        for(int i = 0; i < lights.Length; i++) {
            if(!lights[i] && allLightOn) {
                lights[i] = true;
                if (i + 1 < lights.Length) {
                    allLightOn = false;
                }
                break;
            }
        }
        if (allLightOn) {
            pantalla.GetComponent<cambiarMaterial>().setChangeImage(true);
        }
        return allLightOn;
    }

    public void setScores(int incScore) {
        scores = (scores + incScore) * multiplicador;
    }

    public void setInGame(bool playing) {
        inGame = playing;
    }

    public void setVidas(int life) {
        vidas = life;
    }

    public int getVidas() {
        if(vidas == 0) {
            oneTimeOnly = false;
        }
        return vidas;
    }
}
