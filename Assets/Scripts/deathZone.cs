﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathZone : MonoBehaviour {

    [SerializeField] GameObject GameManager;
    GameObject bola;
    [SerializeField] GameObject muelle;
    [SerializeField] GameObject pared;
    [SerializeField] GameObject fire;
    int vidas;
    private bool positionRecogida;
    private Vector3 bolaPosition;


	// Use this for initialization
	void Start () {
        positionRecogida = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(!positionRecogida && GameObject.FindGameObjectWithTag("bola") != null) {
            bola = GameObject.FindGameObjectWithTag("bola");
            bolaPosition = bola.transform.position;
            positionRecogida = true;
        }
	}

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "bola") {
            vidas = GameManager.GetComponent<GameManager>().getVidas();
            vidas--;
            bola.transform.position = bolaPosition;
            pared.gameObject.GetComponent<Collider>().isTrigger = true;
            pared.gameObject.GetComponent<MeshRenderer>().enabled = false;
            if(vidas <= 0) {
                bola.SetActive(false);
            }
            GameManager.GetComponent<GameManager>().setVidas(vidas);
            muelle.GetComponent<muelle>().outBall = false;
            fire.GetComponent<desactivarse>().contador = 0;
        }
    }
}
