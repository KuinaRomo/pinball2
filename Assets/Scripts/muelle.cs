﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class muelle : MonoBehaviour {

    private float initialPosition;
    private float finalPosition;
    public bool pressed;
    public bool shoot;
    public bool outBall;
    public bool inPosition;
    private float contador;
    public float force;
    [SerializeField] GameObject fire;
    [SerializeField] GameObject GameManager;
    private GameObject bola;
    public bool bolaIn;

	// Use this for initialization
	void Start () {
        initialPosition = 0.40f;
        finalPosition = 0.47f;
        pressed = false;
        shoot = false;
        outBall = false;
        inPosition = false;
        bolaIn = false;
        contador = 0.6f;
        //bola = GameObject.FindGameObjectWithTag("bola");
        force = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if(GameObject.FindGameObjectWithTag("bola") != null) {
            if (!bolaIn) {
                bola = GameObject.FindGameObjectWithTag("bola");
                bolaIn = true;
            }
            if(bola.transform.position.z - 0.6f <= this.transform.position.z 
                && GameManager.GetComponent<GameManager>().getVidas() > 0) {
                inPosition = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && GameManager.GetComponent<GameManager>().getVidas() > 0) {
            if (inPosition && !outBall) {
                GetComponent<AudioSource>().Play();
                pressed = true;
            }
        }
        else if(Input.GetKeyUp(KeyCode.Space) && GameManager.GetComponent<GameManager>().getVidas() > 0) {
            if (inPosition && !outBall) {
                pressed = false;
                shoot = true;
            }
        }
    }

    void FixedUpdate() {
        if (pressed && !shoot) {
            if (contador > 0.001f && transform.position.z > 695) {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2);
                if (transform.position.z < 695) {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 695);
                }
                contador = 0;
            }
            else {
                contador += Time.deltaTime;
            }

        }

        if (shoot) {
            if (contador > 0.01f && transform.position.z < 700) {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 3);
                if (transform.position.z > 700) {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 700);
                }
                contador = 0;
                force += 50;
            }
            else if (transform.position.z < 700) {
                contador += Time.deltaTime;
            }
            else {
                fire.SetActive(true);
                bola.GetComponent<Rigidbody>().AddForce(0, 0, force, ForceMode.Impulse);
                inPosition = false;
                shoot = false;
                force = 0;
            }
        }

        if (!pressed && !shoot && transform.position.z < 700) {
            if (contador > 0.01f) {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 3);
                if (transform.position.z > 700) {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 700);
                }
                contador = 0;
            }
            else {
                contador += Time.deltaTime;
            }
        }
    }
}
