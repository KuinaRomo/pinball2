﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emissionOn : MonoBehaviour {
    private bool lightsOn;
    private float contadorLights;
    [SerializeField] Material light;
    [SerializeField] GameObject GameManager;

    // Use this for initialization
    void Start () {
        light.DisableKeyword("_EMISSION");
    }
	
	// Update is called once per frame
	void Update () {
		if(contadorLights > 0.2f) {
            light.DisableKeyword("_EMISSION");
            contadorLights = 0;
            lightsOn = false;
        }
        if (lightsOn) {
            contadorLights += Time.deltaTime;
        }
	}

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "bola") {
            GameManager.GetComponent<GameManager>().setScores(10);
            lightsOn = true;
            contadorLights = 0;
            light.EnableKeyword("_EMISSION");
        }
    }
}
