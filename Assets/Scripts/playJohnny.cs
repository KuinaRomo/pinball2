﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playJohnny : MonoBehaviour {

    [SerializeField] GameObject GameManager;
    private bool reseteado;

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.GetComponent<GameManager>().getVidas() <= 0 && !reseteado) {
            GetComponent<AudioSource>().time = 0;
            reseteado = true;
        }
        else if(GameManager.GetComponent<GameManager>().getVidas() > 0 && reseteado) {
            reseteado = false;
        }
	}

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "bola") {
            GetComponent<AudioSource>().Play();
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "bola") {
            GameManager.GetComponent<GameManager>().setScores(1);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "bola") {
            GetComponent<AudioSource>().Pause();
        }
    }
}
