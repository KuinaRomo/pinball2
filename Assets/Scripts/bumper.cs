﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumper : MonoBehaviour {

    Vector3 bolaVector;
    [SerializeField] float force;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate() {
        
    }

    private void OnCollisionEnter(Collision collision){
        
        if(collision.gameObject.tag == "bola") {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(collision.gameObject.transform.position.x * -1, collision.gameObject.transform.position.y * -1, collision.gameObject.transform.position.z) * force);
            GetComponent<AudioSource>().Play();
        }
    }
}
