﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiarMaterial : MonoBehaviour {

    [SerializeField] Material[] filmImages;
    [SerializeField] Material material;
    //[SerializeField] Texture texture;
    //[SerializeField] Texture initialTexture;
    [SerializeField] GameObject GameManager;
    [SerializeField] AudioClip[] audios;
    private bool changeImage;
    private int numMaterial;
    private bool reseteado;

	// Use this for initialization
	void Start () {
        numMaterial = 0;
        //m_Material = material;
    }
	
	// Update is called once per frame
	void Update () {
        if (changeImage) {
            changeMaterialImage();
            changeImage = false;
        }
        if (GameManager.GetComponent<GameManager>().getVidas() <= 0 && !reseteado) {
            GetComponent<Renderer>().material = filmImages[filmImages.Length - 1];
            numMaterial = 0;
            reseteado = true;
        }
        else if (GameManager.GetComponent<GameManager>().getVidas() > 0 && reseteado) {
            reseteado = false;
        }
    }

    public void setChangeImage(bool change) {
        changeImage = change;
        return;
    }

    private void changeMaterialImage() {
        GetComponent<Renderer>().material = filmImages[numMaterial];
        if(numMaterial < 3) {
            GetComponent<AudioSource>().clip = audios[numMaterial];
            GetComponent<AudioSource>().Play();
        }
        if(filmImages.Length - 1 != numMaterial) {
            numMaterial++;
        }
        else {
            numMaterial = 0;
        }
    }
}
