﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bajarTarget : MonoBehaviour {

    [SerializeField] public Material materialLight;
    [SerializeField] GameObject GameManager;
    [SerializeField] GameObject parent;
    GameObject child;
    bool allDown;
    public bool bajado;
    private bool reseteado;
    
	// Use this for initialization
	void Start () {
        reseteado = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.GetComponent<GameManager>().getVidas() <= 0 && !reseteado) {
            resetTargets();
            GameManager.GetComponent<GameManager>().resetFirst = false;
            GameManager.GetComponent<GameManager>().resetTargets();
            reseteado = true;
        }
        else if(GameManager.GetComponent<GameManager>().getVidas() > 0 && reseteado) {
            reseteado = false;
        }
	}

    void resetTargets() {
        if (bajado) {
           materialLight.DisableKeyword("_EMISSION");
           bajado = false;
           transform.position = new Vector3(child.transform.position.x, child.transform.position.y + 2.15f, child.transform.position.z);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "bola") {
            if (!bajado) {
                this.transform.position = new Vector3(transform.position.x, transform.position.y - 2.15f, transform.position.z);
                materialLight.EnableKeyword("_EMISSION");
                allDown = GameManager.GetComponent<GameManager>().putTrue();
                if (allDown == true) {
                    for (int i = 0; i < parent.GetComponent<Transform>().childCount; i = i + 2) {
                        child = parent.GetComponent<Transform>().GetChild(i).gameObject;
                        child.GetComponent<bajarTarget>().materialLight.DisableKeyword("_EMISSION");
                        child.GetComponent<bajarTarget>().bajado = false;
                        child.transform.position = new Vector3(child.transform.position.x, child.transform.position.y + 2.15f, child.transform.position.z);
                    }
                    GameManager.GetComponent<GameManager>().resetTargets();
                }
                else  {
                    bajado = true;
                }
            }
        }
    }
}
