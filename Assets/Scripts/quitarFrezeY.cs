﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quitarFrezeY : MonoBehaviour {

    [SerializeField] bool frezee;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "bola") {
            if (frezee) {
                Debug.Log("Defrezee");
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            }
            else {
                Debug.Log("Frezee");
                other.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
                other.gameObject.GetComponent<Rigidbody>().AddForce(0, 0, 5, ForceMode.Acceleration);
            }
        }
    }
}
