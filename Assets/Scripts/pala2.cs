﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pala2 : MonoBehaviour {

    JointSpring spring;
    HingeJoint hingeJoint;
    [SerializeField] float restPosition = 0F;
    [SerializeField] float pressedPosition = 45F;
    [SerializeField] float flipperStrength = 1000F;
    [SerializeField] float flipperDamper = 100F;
    [SerializeField] float direction;

    // Use this for initialization
    void Start() {
        hingeJoint = GetComponent<HingeJoint>();
        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;

    }

    void FixedUpdate() {
        if (Input.GetKey(KeyCode.LeftArrow) && this.gameObject.tag == "left") {
            GetComponent<AudioSource>().Play();
            spring.targetPosition = pressedPosition;
            hingeJoint.spring = spring;
        }
        else if (this.gameObject.tag == "left") {
            spring.targetPosition = restPosition;
            hingeJoint.spring = spring;
        }
        if (Input.GetKey(KeyCode.RightArrow) && this.gameObject.tag == "right") {
            GetComponent<AudioSource>().Play();
            spring.targetPosition = pressedPosition;
            hingeJoint.spring = spring;
        }
        else if(this.gameObject.tag == "right") {
            spring.targetPosition = restPosition;
            hingeJoint.spring = spring;
        }
    }
}

