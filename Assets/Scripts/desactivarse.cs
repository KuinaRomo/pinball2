﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desactivarse : MonoBehaviour {

    public int contador;

	// Use this for initialization
	void Start () {
        contador = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "bola") {
            contador++;
            if(contador%2 == 0) {
                this.gameObject.SetActive(false);
            }
        }
    }
}
