﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aparecer : MonoBehaviour {

    public bool showShark;
    public float totalScale;
    private float contador;
    [SerializeField] GameObject light;

	// Use this for initialization
	void Start () {
        transform.localScale = new Vector3(0, 0, 0);
        showShark = true;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (showShark) {
            if(transform.localScale.x < totalScale) {
                if(contador < 0.05f) {
                    contador += Time.deltaTime;
                }
                else {
                    transform.localScale = new Vector3(transform.localScale.x + 0.1f, transform.localScale.y + 0.1f, transform.localScale.z + 0.1f);
                    contador = 0;
                }
            }
            else {
                light.SetActive(true);
                showShark = false;
            }
        }
    }
}
